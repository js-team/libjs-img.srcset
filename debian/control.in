Source: libjs-img.srcset
Section: javascript
Priority: optional
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@lists.alioth.debian.org>
Uploaders: Jonas Smedegaard <dr@jones.dk>
Build-Depends: @cdbs@, debhelper (>= 10~)
Standards-Version: 4.1.1
Homepage: https://github.com/weblinc/img-srcset
Vcs-Git: https://anonscm.debian.org/git/collab-maint/libjs-img.srcset.git
Vcs-Browser: https://anonscm.debian.org/git/collab-maint/libjs-img.srcset.git

Package: libjs-img.srcset
Architecture: all
Depends: ${misc:Depends}
Recommends: javascript-common
Description: fast JavaScript polyfill for img srcset
 img.srcset is a lightweight, no nonsense, all browser supporting, fast
 polyfill for img srcset, allowing for lighter yet backwards-compatible
 responsive web design.
 .
 The srcset attribute is an HTML extension for adaptive (a.k.a.
 responsive) images.  More info at <http://www.w3.org/TR/html-srcset/>.
 .
 A polyfill is (in the context of HTML5) Javascript code implementation
 of a functionality often available in modern web browsers, allowing web
 designers to use simpler standards-compliant and declarative code,
 burdening only older/simpler browsers with these fallback snippets.
