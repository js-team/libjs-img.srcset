libjs-img.srcset (2.0.0~20131003~dfsg-4) unstable; urgency=medium

  * Team upload
  * Declare compliance with policy 4.6.2
  * Replace node-uglify by uglifyjs (Closes: #1032552)

 -- Yadd <yadd@debian.org>  Sun, 13 Aug 2023 08:29:40 +0400

libjs-img.srcset (2.0.0~20131003~dfsg-3) unstable; urgency=medium

  * Team upload

  [ Jelmer Vernooĳ ]
  * Migrate repository from alioth to salsa.

  [ Yadd ]
  * Bump debhelper dependency to >= 9, since that's what is used in debian/compat.
  * Bump debhelper from deprecated 9 to 10.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Declare compliance with policy 4.6.1
  * Add "Rules-Requires-Root: no"
  * Use uglifyjs, node node-uglify

 -- Yadd <yadd@debian.org>  Mon, 05 Sep 2022 03:56:02 +0200

libjs-img.srcset (2.0.0~20131003~dfsg-2) unstable; urgency=medium

  * Update watch file:
    + Bump to file format 4.
    + Mention gbp --uscan in usage comment.
    + Mangle filename.
    + List repacksuffix.
    + Use substitution strings.
  * Use debhelper compatibility level 9 (not 8).
  * Modernize Vcs-* fields:
    + Consistently use https protocol.
    + Consistently use git (not gitweb) path.
    + Consistently include .git suffix in path.
  * Declare compliance with Debian Policy 4.1.0.
  * Modernize git-buildpackage config: Filter any .git* file.
  * Modernize cdbs:
    + Do copyright-check in maintainer script (not during build).
  * Update copyright info:
    + Use https protocol in file format URL.
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Extend coverage for myself.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.
  * Use section javascript (not web).

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 14 Sep 2017 17:01:56 +0200

libjs-img.srcset (2.0.0~20131003~dfsg-1) unstable; urgency=low

  * Initial release.
    Closes: Bug#727730.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 30 Oct 2013 15:15:57 +0100
